(require 'package)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
(package-initialize)

;; use-package
(eval-when-compile
  (require 'use-package))

;; Settings set from withing emacs
(custom-set-variables
    ;; custom-set-variables was added by Custom.
    ;; If you edit it by hand, you could mess it up, so be careful.
    ;; Your init file should contain only one such instance.
    ;; If there is more than one, they won't work right.
    '(ansi-color-faces-vector
	[default default default italic underline success warning error])
    '(ansi-color-names-vector
	["black" "#d55e00" "#009e73" "#f8ec59" "#0072b2" "#cc79a7" "#56b4e9" "white"])
    '(custom-enabled-themes nil)
    '(ido-mode (quote both) nil (ido))
    '(inhibit-startup-screen t)
    '(menu-bar-mode nil)
    '(package-selected-packages (quote (lsp-mode multi-term org evil)))
    '(scroll-bar-mode nil)
    '(tool-bar-mode nil)
    '(uniquify-buffer-name-style (quote post-forward) nil (uniquify))
)

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
 
(use-package doom-themes
    :ensure t
    :config
    ;; Global settings (defaults)
    (setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
	doom-themes-enable-italic t) ; if nil, italics is universally disabled
    (load-theme 'doom-moonlight t)
    ;;(load-theme 'doom-dracula t)
    ;;(load-theme 'doom-laserwave t)
    ;;(load-theme 'doom-outrun-electric t)
    ;;(load-theme 'doom-one t)

    ;; Enable flashing mode-line on errors
    (doom-themes-visual-bell-config)
    ;; Enable custom neotree theme (all-the-icons must be installed!)
    (doom-themes-neotree-config)
    ;; or for treemacs users
    (setq doom-themes-treemacs-theme "doom-atom") ; use "doom-colors" for less minimal icon theme
    (doom-themes-treemacs-config)
    ;; Corrects (and improves) org-mode's native fontification.
    (doom-themes-org-config)
)

;; Paste to cursor position
(setq mouse-yank-at-point t)

;;; From https://emacs.stackexchange.com/questions/30137/paste-windows-clipboard-with-shift-insert-but-not-by-yanking
;; Paste from clipboard
(global-set-key (kbd "<S-Insert>") #'clipboard-yank)

;; Switch to previous buffer
(global-set-key (kbd "M-o") 'mode-line-other-buffer)

;; Display line numbers
(global-linum-mode 1)

(electric-pair-mode t)

;; evil mode - vim like mode
(use-package evil
    :ensure t
    :commands evil-mode
    :diminish evil-mode
    :init
	;; Enable evil mode
	(evil-mode 1)

	;; Persistent highlights on evil mode search
	(evil-select-search-module 'evil-search-module 'evil-search)

	;; Enables evil mode for other modes
	(setq evil-insert-state-modes nil)
	(setq evil-motion-state-modes nil)

	;; Make searches case sensitive
	(setq evil-ex-search-case 'sensitive)

	;; Disable 'K' key binding. This was determined by "C-h K"
	( define-key evil-motion-state-map "K" nil)
)

(use-package evil-nerd-commenter
    :ensure t
    :bind ("M-/" . evilnc-comment-or-uncomment-lines)
)

(use-package avy
    :ensure t
    :config
	(global-set-key (kbd "C-:") 'avy-goto-char)
	(global-set-key (kbd "C-'") 'avy-goto-char-2)
	(global-set-key (kbd "C-;") 'avy-goto-char-timer)
	(global-set-key (kbd "M-g f") 'avy-goto-line)
	(global-set-key (kbd "M-g w") 'avy-goto-word-1)
	(global-set-key (kbd "M-g e") 'avy-goto-word-0)
)

(use-package doom-modeline
    :ensure t
    :init 
	(doom-modeline-mode 1)
	(use-package all-the-icons)
)
  
(use-package nyan-mode
    :ensure t
    :config
	(nyan-mode 1)
	(setq nyan-animate-nyancat t)
)

(defun efs/lsp-mode-setup ()
    (setq lsp-headerline-breadcrumb-segments '(path-up-to-project file symbols))
    (lsp-headerline-breadcrumb-mode)
)

(use-package lsp-mode
    :ensure t
    :init
    ;; set prefix for lsp-command-keymap (few alternatives - "C-l", "C-c l")
    (setq lsp-keymap-prefix "C-c l")
    :hook (;; replace XXX-mode with concrete major-mode(e. g. python-mode)
	    (prog-mode . lsp)
	    ;; if you want which-key integration
	    (lsp-mode . lsp-enable-which-key-integration)
	    (lsp-mode . efs/lsp-mode-setup))
    :commands lsp
)

;; optionally
(use-package lsp-ui 
    :ensure t 
    :hook 
	(lsp-mode . lsp-ui-mode)
    :custom
	(lsp-ui-doc-position 'bottom)
    :commands lsp-ui-mode
)

;; flycheck
(use-package flycheck
    :ensure t
)

;; if you are helm user
(use-package helm-lsp 
    :ensure t 
    :commands helm-lsp-workspace-symbol
)

;;(use-package helm)
;; if you are ivy user
(use-package lsp-ivy 
    :ensure t
    :commands lsp-ivy-workspace-symbol
)

;; counsel install ivy and swiper as dependencies
(use-package counsel
    :ensure t
    :config
	(ivy-mode 1)
	(setq ivy-use-virtual-buffers t)
	(setq ivy-count-format "(%d/%d) ")
)

;; lsp-treemacs
(use-package lsp-treemacs
    :ensure t
    :commands lsp-treemacs-errors-list
)

;; optionally if you want to use debugger
(use-package dap-mode
    :ensure t
)
;; (use-package dap-LANGUAGE) to load the dap adapter for your language

;; optional if you want which-key integration
(use-package which-key
    :ensure t
    :config
	(which-key-mode)
)

(use-package company
    :ensure t
    :after lsp-mode
    :hook (lsp-mode . company-mode)
    :bind
	(:map company-active-map
	    ("<tab>" . company-complete-selection)
	)
	(:map lsp-mode-map
	    ("<tab>" . company-indent-or-complete-common)
	)
    :custom
	(company-minimum-prefix-length 1)
	(company-idle-delay 0.0)
)

(use-package company-box
  :ensure t
  :hook 
	(company-mode . company-box-mode)
)

(use-package typescript-mode
    :mode "\\.ts\\'"
    :hook 
	(typescript-mode . lsp-deferred)
    :config
	(setq typescript-indent-level 2)
)

(use-package org
    :ensure t
    :init
    (use-package toc-org
	:ensure t
	:init
	(if (require 'toc-org nil t)
	    (progn
		(add-hook 'org-mode-hook 'toc-org-mode)
		;; enable in markdown, too
		(add-hook 'markdown-mode-hook 'toc-org-mode)
		(define-key markdown-mode-map (kbd "\C-c\C-o") 'toc-org-markdown-follow-thing-at-point))
	(warn "toc-org not found"))
	(toc-org-mode 1)
    )
)
